import React, { useState, useEffect } from 'react';
import '../../css/Presentation.css'; // Importez le fichier CSS pour les styles de la bannière de présentation
import '../../css/TextArea.css';

function BannerCompetences() {
  const [bannerOpacity, setBannerOpacity] = useState(0);
  const [contentBannerPres, setContentBannerPres] = useState('150%');

  useEffect(() => {
    setBannerOpacity(1);
  }, []);

  useEffect(() => {
    const timer = setTimeout(() => {
      setContentBannerPres('0%'); // Réinitialiser la marge après un certain délai
    }, 100); // Modifier ce délai selon vos besoins

    return () => clearTimeout(timer); // Nettoyer le timer lors du démontage du composant
  }, []);

  return (
    <div className="presentation-banner" style={{ opacity: bannerOpacity }}>
      <div className="ContentBannerPres" style={{ marginLeft: contentBannerPres }}>
        <div className="TextArea">
          <h2>Salut à toi !</h2>
          <p>
            <br />
            Tu trouveras ici toutes mes passions, mes explorations et éxpérimentations dans l'univers infini de l'informatique.
            <br />
            Dans un esprit DIY, comme un raton laveur farfouillant dans les recoins les moins explorés, je déniche des trésors là où d'autres voient juste des déchets. Mon objectif ? Transformer ces trouvailles en quelque chose de nouveau, d'utile, bref créer.
          </p>
        </div>
        <div className="TextArea">
          <h2>Je suis la pour travailler AVEC !</h2>
          <p>
            Je suis également la pour présenter mes capacités et proposer mes services. J'adore travailler avec d'autres passionnés, échanger des idées, et participer pleinement à la création de projets inspirants. Que tu sois un amateur curieux, un professionnel chevronné ou simplement quelqu'un qui aime bidouiller, je suis toujours partant pour partager mes connaissances, apprendre de nouvelles choses et contribuer à des initiatives innovantes. Alors, n'hésite pas à me contacter si tu as des idées à partager ou si tu souhaites collaborer sur un projet ensemble !
          </p>
        </div>
      </div>
    </div>
  );
}

export default BannerCompetences;
