import React, { useEffect, useState, } from 'react';
import Banner from '../../../Banner';
import BannerOCProjetsA4 from './BannerOCProjetsA4';
import { useLocation } from 'react-router-dom';


function OCa4() {

  return (
<main>
        <Banner>
          <h1>Créez une application web avec React</h1>
        </Banner>
         <div>
          <BannerOCProjetsA4 />
        </div>
</main>

  );
}

export default OCa4;
