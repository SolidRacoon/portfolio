import React, { useEffect, useState, } from 'react';
import Banner from '../../../Banner';
import BannerOCProjetsA1 from './BannerOCProjetsA1';
import { useLocation } from 'react-router-dom';


function OCa1() {

  return (
<main>
        <Banner>
          <h1>Créez une application web avec React</h1>
        </Banner>
         <div>
          <BannerOCProjetsA1 />
        </div>
</main>

  );
}

export default OCa1;

